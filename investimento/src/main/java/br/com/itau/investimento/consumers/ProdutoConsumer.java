package br.com.itau.investimento.consumers;

import java.util.List;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import br.com.itau.investimento.models.Produto;

@FeignClient(name = "produto", path = "/produto")
public interface ProdutoConsumer {

	@GetMapping
	List<Produto> listar();

	@GetMapping("/{id}")
	Produto buscar(@PathVariable("id") int id);
}
