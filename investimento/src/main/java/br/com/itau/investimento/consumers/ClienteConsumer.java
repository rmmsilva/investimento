package br.com.itau.investimento.consumers;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import br.com.itau.investimento.models.Cliente;

@FeignClient(name = "cliente", path = "/cliente")
public interface ClienteConsumer {

	@GetMapping("/{cpf}")
	Cliente buscar(@PathVariable("cpf") String cpf);
}
