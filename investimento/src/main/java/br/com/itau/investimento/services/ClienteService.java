package br.com.itau.investimento.services;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.itau.investimento.consumers.ClienteConsumer;
import br.com.itau.investimento.models.Cliente;

@Service
public class ClienteService {

	@Autowired
	ClienteConsumer clienteConsumer;

	public Optional<Cliente> buscar(String cpf) {
		return Optional.ofNullable(clienteConsumer.buscar(cpf));
	}

}
