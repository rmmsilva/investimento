package br.com.itau.investimento.services;

import java.util.Optional;

import org.springframework.stereotype.Service;

import br.com.itau.investimento.consumers.ProdutoConsumer;
import br.com.itau.investimento.models.Produto;

@Service
public class ProdutoService {

	private final ProdutoConsumer produtoConsumer;

	public ProdutoService(ProdutoConsumer produtoConsumer) {
		this.produtoConsumer = produtoConsumer;
	}

	public Iterable<Produto> listar() {
		return produtoConsumer.listar();
	}

	public Optional<Produto> buscar(int id) {
		return Optional.ofNullable(produtoConsumer.buscar(id));
	}

}
