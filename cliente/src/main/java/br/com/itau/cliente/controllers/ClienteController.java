package br.com.itau.cliente.controllers;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.itau.cliente.models.Cliente;
import br.com.itau.cliente.services.ClienteService;

@RestController
@RequestMapping("/cliente")
public class ClienteController {
	@Autowired
	ClienteService clienteService;

	@PostMapping
	public ResponseEntity<Cliente> criar(@RequestBody Cliente cliente) {
		cliente = clienteService.cadastrar(cliente);

		return ResponseEntity.status(201).body(cliente);
	}

	@GetMapping("/{cpf}")
	public ResponseEntity<Optional<Cliente>> buscar(@PathVariable String cpf) {
		return ResponseEntity.status(201).body(clienteService.buscar(cpf));
	}
}
