package br.com.itau.produto.controllers;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.itau.produto.models.Produto;
import br.com.itau.produto.services.ProdutoService;

@RestController
@RequestMapping("/produto")
public class ProdutoController {
	@Autowired
	ProdutoService produtoService;

	@GetMapping
	public Iterable<Produto> listar() {
		return produtoService.listar();
	}

	@GetMapping("/{id}")
	public Optional<Produto> buscar(@PathVariable("id") int id) {
		return produtoService.buscar(id);
	}

}
